    //We'll need to create all our waypoints when the page loads
    //create a new Waypoint object 
    new Waypoint({
        //assign an element that will trigger the event
        element: document.getElementById('basic-waypoint'), 
        //handle the event by creating a funciton that gets called when the waypoint is reached
        handler: function(direction) { 
            //do this stuff when the waypoint is reached
            alert("This alert was triggered by scrolling " + direction + " to '" + this.element.id + "'. The trigger point is at " + this.triggerPoint + "px from the top of the page.");
        },
        //optionally, you can add an offset to trigger the event a specified number of pixels before the element
        offset: 100
    })

    //Let's make more waypoints!

    /*We'll be having all our actions happen in the same element on our page. 
      So let's store that element in a variable that we can reuse.*/
    const boxElement = document.getElementById('box');
    const xRayElement = document.getElementById('x-ray');
    const skullElement = document.getElementById('skull');

    //add a class to hide some of the elements so we can animate them later
    //this could have also been done by editing the element tags in HTML
    xRayElement.classList.add('hidden');
    skullElement.classList.add('hidden');

    //Waypoint #1
    new Waypoint({
        element: document.getElementById('step-1'), 
        handler: function(direction) { 
            //we ony want this event to trigger when we scroll down so let's add a conditional statement here         
            if(direction === 'down')
            {
                //we're going to add a class called 'is-fixed'
                boxElement.classList.add('is-fixed');
            }
            else
            {
                //when we scroll up, we'll remove the class
                boxElement.classList.remove('is-fixed');
            }

        },
        //offsetting by the same value as the top property in our 'is-fixed' class. 
        //this ensures our element doesn't jump around when the waypoint is triggered.
        offset: '25%' 
    })
    
    //Waypoint #2
    //animate in the x-ray element using a bounce entrance and exit from animate.css
    new Waypoint({
        element: document.getElementById('step-2'), 
        handler: function(direction) {        
            if(direction === 'down')
            {
                //remove any classes that we already added to the element
                xRayElement.classList.remove('animate__bounceOutDown', 'hidden');
                //add classes from animate.css. the 'animate__animated' is needed to play the animation
                xRayElement.classList.add('animate__animated', 'animate__bounceInUp');
            }
            else
            {
                //swap the classes we add and remove to reverse the animation
                xRayElement.classList.remove('animate__bounceInUp');
                xRayElement.classList.add('animate__animated','animate__bounceOutDown'); //we don't need to add 'hidden' back to the element since we've animated it offscreen
            }

        },
        offset: '25%' 
    })

    //Waypoint #3
    ////animate in the skull element using a fade entrance and exit from animate.css
    new Waypoint({
        element: document.getElementById('step-3'), 
        handler: function(direction) {        
            if(direction === 'down')
            {
                skullElement.classList.remove('animate__fadeOut', 'hidden');
                skullElement.classList.add('animate__animated', 'animate__fadeIn');
            }
            else
            {
                skullElement.classList.remove('animate__fadeIn');
                skullElement.classList.add('animate__animated','animate__fadeOut');
            }

        },
        offset: '25%' 
    })

    //Waypoint #4
    //as the user scrolls away from the contatiner element we'll stop the box from following along
    new Waypoint({
        element: document.getElementById('step-4'), 
        handler: function(direction) {        
            if(direction === 'down')
            {
                boxElement.classList.remove('is-fixed');
                boxElement.classList.add('is-bottom');
            }
            else
            {
                boxElement.classList.remove('is-bottom');
                boxElement.classList.add('is-fixed');
            }

        },
        offset: '25%' 
    })